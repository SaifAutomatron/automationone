package V1;

import java.util.ArrayList;
import java.util.List;

import org.testng.TestNG;
import org.testng.collections.Lists;

import Utilities.Listener.ExtentListener;

public class TestRunner {
	
	static TestNG testNG;
	
	public static void main(String[] args) {
		ExtentListener extent=new ExtentListener();
		
		testNG=new TestNG();
		List<String> suites = Lists.newArrayList();
		suites.add("./Suites/UITest.xml");
		testNG.setTestSuites(suites);
		testNG.run();
	}

}
